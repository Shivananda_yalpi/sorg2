trigger AccountAddressTrigger on Account (before insert,before update) {
	list<account> acc=trigger.new;
    for(account a:acc){
        if(a.Match_Billing_Address__c==true){
            if(a.BillingPostalCode!=null){
                a.ShippingPostalCode=a.BillingPostalCode;
            }
        }
    }
}