trigger ClosedOpportunityTrigger on Opportunity (before insert,before update) {
	list<Opportunity> oppList=trigger.new;
    List<Task> tasks = new List<Task>();
    for(Opportunity opp:oppList){
        if(opp.StageName=='Closed Won'){
            Task tsk = new Task(whatID = Opp.ID,subject='Follow Up Test Task'); 
            tasks.add(tsk);
            
        }
    }
    insert tasks;
    
}