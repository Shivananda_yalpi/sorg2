public class ContactAndLeadSearch {
    public static List<List<sObject>> searchContactsAndLeads(string a){
        
        List<List<sObject>> alllist=[find 'smith' in name fields returning lead(name where lastname=:a or firstname=:a),contact(name where lastname=:a or firstname=:a)];
        
        return alllist;
    }
}