public class AnimalLocator {
	
    public static String getAnimalNameById(Integer id){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        String endpoint='https://th-apex-http-callout.herokuapp.com/animals/'+string.valueOf(id);
        request.setEndpoint(endpoint);
        
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        
            // Deserializes the JSON string into collections of primitive data types.
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            // Cast the values in the 'animals' key as a list
            Map<string,object> animals = (Map<string,object>) results.get('animal');
            string a=(string)animals.get('name');
            System.debug(a);
            return a;
        
    
    }
    
}