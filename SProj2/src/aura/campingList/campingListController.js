({
	ensureNotNull : function(component, event, helper) {
		var cmpNameField=component.find("cmpName");
        var cmpName=cmpNameField.get("v.value");
        var cmpPriceField=component.find("cmpPrice");
        var cmpPrice=cmpPriceField.get("v.value");
        var cmpQuantityField=component.find("cmpQuantity");
        var cmpQuantity=cmpQuantityField.get("v.value");
        
        var validity=true;
        if($A.util.isEmpty(cmpName)){
            cmpNameField.set("v.errors",[{message:"name Cannot be null"}]);
            validity=false;
        }
        else if($A.util.isEmpty(cmpQuantity)){
            cmpQuantityField.set("v.errors",[{message:"quantity cannot be null"}]);
            validity=false;
        }
        else if($A.util.isEmpty(cmpPrice)){
            cmpPriceField.set("v.errors",[{message:"Price cannot be null"}]);    
            validity=false;
        }
        else{
            cmpNameField.set("v.errors",null);
            cmpQuantityField.set("v.errors",null);
            cmpPriceField.set("v.errors",null);
            validity=true;
        }
        if(validity==true){
            var cmplist=component.get("v.items");
            var cmpItem=component.get("v.newItem");
            
            console.log("Create expense: " + JSON.stringify(cmpItem));
            
            cmplist.push(cmpItem);
            component.set("v.items",cmplist);
            component.set("v.newItem",{ 'sobjectType': 'Camping_Item__c',
                    'Name': '',
                    'Quantity__c': 0,
                    'Price__c': 0,
                    'Packed__c': false })

            
        }
	}
})